# # FROM panteparak/wait-for as temp

# FROM openjdk:8-alpine

# RUN apk add --update --no-cache bash tar

# COPY --from=temp /wait-for-it /bin/wait-for-it
# RUN chmod +x /bin/wait-for-it

# RUN ls -la /bin | grep wait-for-it

# CMD target/scala-2.12/extractor-worker-assembly-0.1.jar



FROM openjdk:8-alpine
RUN apk add --update --no-cache bash tar
WORKDIR /app
COPY target/scala-2.12/extractor-worker-assembly-0.1.jar extractor-worker.jar
CMD java -Xms512m -Xmx2g -jar extractor-worker.jar
