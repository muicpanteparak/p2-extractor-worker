package main

import java.io.{File, FileInputStream}

import com.google.gson.JsonObject
import me.teparak.minio.utils.MinioWrapper
import me.teparak.rabbit.utils.Rabbit
import org.apache.commons.io.{FileUtils, FilenameUtils}

import scala.util.Properties.envOrElse

object Main extends App {
  private val MINIO_HOST = envOrElse("MY_MINIO_HOST", "localhost")
  private val MINIO_PORT = envOrElse("MY_MINIO_PORT", "9000").toInt
  private val MINIO_USER = envOrElse("MY_MINIO_USER", "admin")
  private val MINIO_PASS = envOrElse("MY_MINIO_PASS", "password")


  private val RABBIT_HOST = envOrElse("MY_RABBIT_HOST", "localhost")
  private val RABBIT_PORT = envOrElse("MY_RABBIT_PORT", "5672").toInt
  private val RABBIT_USER = envOrElse("MY_RABBIT_USER", "guest")
  private val RABBIT_PASS = envOrElse("MY_RABBIT_PASS", "guest")

  var rab = new Rabbit(RABBIT_HOST, RABBIT_PORT, RABBIT_USER, RABBIT_PASS)


  println(MINIO_HOST, MINIO_PORT, MINIO_USER, MINIO_PASS)
  println(RABBIT_HOST, RABBIT_PORT, RABBIT_USER, RABBIT_PASS)
  val minio = new MinioWrapper(MINIO_HOST, MINIO_PORT, MINIO_USER, MINIO_PASS)
  val QUEUE_NAME = "extract"

  def cbb(jobId: String, objectId: String, filename: String, status: Boolean): Unit = {
    println("Notify status and pipeline")

    val json = new JsonObject

    println(objectId)

    json.addProperty("jobId", jobId)
    json.addProperty("objectId", objectId)
    json.addProperty("jobType", "extract")
    json.addProperty("status", status)
    json.addProperty("fileName", filename)

    rab.publish(json, "status")

    val js = new JsonObject
    js.addProperty("jobId", jobId)
    js.addProperty("objectId", objectId)
    js.addProperty("fileName", filename)

    rab.publish(js, "parse")
  }

  def completeAlert(jobId: String): Unit = {
    val json = new JsonObject

    println()

    json.addProperty("jobId", jobId)
//    json.addProperty("objectId", objectId)
    json.addProperty("jobType", "extract_complete")
    json.addProperty("status", true)
//    json.addProperty("fileName", filename)

    rab.publish(json, "status")
  }

  def cb(js: JsonObject): Unit = {

    val jsonInit = new JsonObject
    // Get jobId, objectId from json object
    val jobId = js.get("jobId").getAsString
    val objectId = js.get("objectId").getAsString
    // download file from Minio
    println(s"Job: $jobId , Object: $objectId")

    val fi = minio.download(jobId, objectId)
    val file: File = File.createTempFile("extract-job", ".tar.gz", new File("/tmp"))
    println(file.getPath)
    FileUtils.copyInputStreamToFile(fi, file)
    fi.close()

    TarExtractor
      .listFile(new FileInputStream(file))
      .foreach(s => {

        val json = new JsonObject

        println(objectId)

        json.addProperty("jobId", jobId)
        json.addProperty("objectId", objectId)
        json.addProperty("jobType", "extract")
        json.addProperty("status", false)
        json.addProperty("fileName", FilenameUtils.getName(s))

        rab.publish(json, "status")
      })
    TarExtractor.untar(new FileInputStream(file), jobId, cbb)

    completeAlert(jobId)
    file.delete()
  }


  println(f"Listening to $QUEUE_NAME")
  rab.listen(QUEUE_NAME, cb _)
}
