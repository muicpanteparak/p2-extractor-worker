package main

import java.io._

import me.teparak.minio.utils.MinioWrapper
import org.apache.commons.compress.archivers.tar.{TarArchiveEntry, TarArchiveInputStream}
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream
import org.apache.commons.io.{FileUtils, FilenameUtils}

import scala.util.Properties.envOrElse

object TarExtractor {

  private val MINIO_HOST = envOrElse("MY_MINIO_HOST", "localhost")
  private val MINIO_PORT = envOrElse("MY_MINIO_PORT", "9000").toInt
  private val MINIO_USER = envOrElse("MY_MINIO_USER", "admin")
  private val MINIO_PASS = envOrElse("MY_MINIO_PASS", "password")

  val BUFFER = 2048
  val TEMP_DIR = "/tmp/"
  val PDF_FOLDER = "pdf/"
  val minio = new MinioWrapper(MINIO_HOST, MINIO_PORT, MINIO_USER, MINIO_PASS)


  def validateFileName(name: String): Boolean = !FilenameUtils.getName(name).startsWith(".") && !FilenameUtils.getName(name).trim().isEmpty


  def listFile(in: InputStream): Vector[String] = {
    val gzIn: GzipCompressorInputStream = new GzipCompressorInputStream(in)
    val tarInput: TarArchiveInputStream = new TarArchiveInputStream(gzIn)

    var entry: TarArchiveEntry = tarInput.getNextTarEntry
    var ret = Vector[String]()
    while (tarInput.getNextTarEntry != null){
      ret = ret :+ tarInput.getCurrentEntry.getName
      entry = tarInput.getNextTarEntry
    }

    ret
      .filter(validateFileName)
      .map(FilenameUtils.getName)
  }

  def untar(in: InputStream, jobId: String, cb: (String, String, String, Boolean) => Unit) {
    def processTar(tarIn: TarArchiveInputStream): Unit = {

      def processFileInTar(dest: BufferedOutputStream, path: String): Unit = {
        val data = new Array[Byte](BUFFER)
        val count = tarIn.read(data, 0, BUFFER)
        count match {
          case -1 =>
            dest.close()
          case _ =>
            dest.write(data, 0, count)
            processFileInTar(dest, path)
        }
      }

      tarIn.getNextEntry.asInstanceOf[TarArchiveEntry] match {
        case null =>

        case a: TarArchiveEntry if a.isFile && validateFileName(a.getName) =>


          println("Extracting: " + a.getName)
          val path = TEMP_DIR + a.getName
          val fi = new File(path)

          FileUtils.touch(fi)

          val fos: FileOutputStream = new FileOutputStream(path)
          val dest: BufferedOutputStream = new BufferedOutputStream(fos, BUFFER)

          processFileInTar(dest, path)

          val fn = FilenameUtils.getName(fi.getName)

          val objectId = PDF_FOLDER + fn
          minio.upload(jobId, objectId, fi)
          println("File upload")
          cb(jobId, objectId, fn, true)
          processTar(tarIn)
        case a: TarArchiveEntry => {
          println("name:",tarIn.getCurrentEntry.getName)
          processTar(tarIn)
        }
      }
    }

    val gzIn: GzipCompressorInputStream = new GzipCompressorInputStream(in)
    val tarIn: TarArchiveInputStream = new TarArchiveInputStream(gzIn)


//    println(listFile(tarIn))
    processTar(tarIn)
    tarIn.close()
    println("Untar completed successfully!!")
  }

  //  untar(new FileInputStream(new File("sample/pdf.tar.gz")),"sample/")
}
