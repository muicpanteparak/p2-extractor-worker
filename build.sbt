name := "extractor-worker"

version := "0.1"

scalaVersion := "2.12.7"
libraryDependencies += "org.apache.commons" % "commons-compress" % "1.6"
resolvers += "Artifactory" at "https://artifact.teparak.me/artifactory/backendtech/"
// https://mvnrepository.com/artifact/com.rabbitmq/amqp-client
//libraryDependencies += "com.rabbitmq" % "amqp-client" % "5.4.3"

libraryDependencies += "me.teparak" % "rabbitmq" % "1.1.3"

// https://mvnrepository.com/artifact/com.google.code.gson/gson
libraryDependencies += "com.google.code.gson" % "gson" % "2.8.5"

// https://mvnrepository.com/artifact/org.mongodb/bson
//libraryDependencies += "org.mongodb.scala" %% "mongo-scala-driver" % "2.4.2"
//libraryDependencies += "org.mongodb" % "bson" % "3.8.2"

// https://mvnrepository.com/artifact/io.minio/minio
libraryDependencies += "me.teparak" % "minio" % "1.2.1"

// https://mvnrepository.com/artifact/commons-io/commons-io
libraryDependencies += "commons-io" % "commons-io" % "2.6"
